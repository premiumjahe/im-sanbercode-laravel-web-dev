<?php
require('Animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Name : ". $sheep->name ."<br>"; 
echo "Legs : ". $sheep->legs ."<br>"; 
echo "cold blooded : ". $sheep->cold_blooded ."<br>"; 

echo "<br>";
echo "<br>";

$frog = new Frog("buduk");
echo "Name : ". $frog->name ."<br>"; 
echo "Legs : ". $frog->legs ."<br>"; 
echo "cold blooded : ". $frog->cold_blooded ."<br>"; 
echo "Jump : ". $frog->jump()."<br>";

echo "<br>";
echo "<br>";

$ape = new Ape("buduk");
echo "Name : ". $ape->name ."<br>"; 
echo "Legs : ". $ape->legs ."<br>"; 
echo "cold blooded : ". $ape->cold_blooded ."<br>"; 
echo "Yell : ". $ape->yell()."<br>";
?>
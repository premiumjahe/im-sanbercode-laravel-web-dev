<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'signup']);
Route::post('/welcome', [AuthController::class, 'send']);
route::get('/data-table', function(){
    return view('data-table');
});
route::get('/table', function(){
    return view('table');
});

route::get('/cast/create', [castController::class, 'create']);
route::post('/cast', [castController::class, 'store']);
route::get('/cast', [castController::class, 'index']);
route::get('/cast/{id}',[castController::class, 'detail']);
route::get('/cast/{id}/edit', [castController::class, 'update']);
route::put('/cast/{id}', [castController::class, 'edit']);
route::delete('/cast/{id}',[castController::class, 'remove']);




<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class castController extends Controller
{
    public function create()
    {
        return view('cast.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50|min:4',
            'age' => 'required',
            'bio' => 'required|max:255'
        ]);

        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'age' => $request->input('age'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.show', ['cast' => $cast]);

    }

    public function detail($id)
    {
        $cast = db::table('cast')->find($id);
        return view('cast.detail', ['cast' => $cast]);
    }

    public function update($id)
    {
        $cast = db::table('cast')->find($id);
        return view('cast.update', ['cast' => $cast]);
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:20|min:4',
            'age' => 'required',
            'bio' => 'required|max:255',
        ]);

        DB::table('cast')
        ->where('id', $id)
        ->update([
            'name' => $request->input('name'),
            'age' => $request->input('age'),
            'bio' => $request->input('bio')
        ]);
        return redirect('/cast');
    }

    public function remove($id)
    {
        db::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}

@extends("master")
@section('title')
Buat Account Baru!
@endsection
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <div>
            <label for="firstName">First Name :</label> <br>
            <input name="firstName" type="text">
        </div> <br>
        <div>
            <label for="lastName">Last Name :</label> <br>
            <input name="lastName" type="text">
        </div> <br>
        <div>
            <label for="">Gender :</label> <br>
            <input type="radio"> Male <br>
            <input type="radio"> Female <br>
            <input type="radio"> Other <br>
        </div> <br>
        <div>
            <label for="">Nationality</label> <br>
            <select name="" id="">
                <option value="">Indonesia</option>
                <option value="">Korea</option>
                <option value="">Jepang</option>
            </select>  
        </div> <br>
        <div>
            <label for="">Language Spoken :</label> <br>
            <input type="checkbox"> Bahasa Indonesia <br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br>
        </div> <br>
        <div>
            <label for="">Bio :</label> <br>
            <textarea name="" id="" cols="30" rows="10"></textarea> <br>
            <button>Sign Up</button>
        </div>
    </form>
@endsection
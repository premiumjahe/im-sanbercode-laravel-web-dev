@extends("master")
@section('title')
detail cast
@endsection
@section('content')
<h1> {{$cast->name}} </h1>
<h6> {{$cast->age}} </h6>
<p> {{$cast->bio}} </p>

<a href="/cast" class="btn btn-warning">back</a>
@endsection

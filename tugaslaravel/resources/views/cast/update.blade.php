@extends("master")
@section('title')
update cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
  <div class="form-group my-4">
    <label >name</label>
    <input name="name" value="{{$cast->name}}" type="text" class="@error('name') is-invalid @enderror form-control">
  </div>
  @error('name')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror

  <div class="form-group">
    <label>age</label>
    <input name="age"type="number" value="{{$cast->age}}" class="@error('name') is-invalid @enderror form-control">
  </div>
  @error('age')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror

  <div class="form-group my-4">
    <label >bio</label>
    <textarea name="bio" type="text" class="@error('name') is-invalid @enderror form-control">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror


  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
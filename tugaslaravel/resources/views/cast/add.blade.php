@extends("master")
@section('title')
add cast
@endsection
@section('content')
<form action="/cast" method="post">
    @csrf

  <div class="form-group">
    <label >name</label>
    <input name="name" type="text" class="@error('name') is-invalid @enderror form-control">
  </div>
  @error('name')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror

  <div class="form-group">
    <label>age</label>
    <input name="age"type="number" class="@error('name') is-invalid @enderror form-control">
  </div>
  @error('age')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror

  <div class="form-group my-4">
    <label >bio</label>
    <textarea name="bio" class="@error('name') is-invalid @enderror form-control"></textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{$message}}</div>
  @enderror


  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
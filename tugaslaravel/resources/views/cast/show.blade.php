@extends("master")
@section('title')
show cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm" my-2>add</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">no</th>
      <th scope="col">name</th>
      <th scope="col">age</th>
      <th scope="col">bio</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
    <tr>
        <th scope="row">{{$key + 1}}</th>
        <td> {{$item->name}} </td>
        <td> {{$item->age}} </td>
        <td>{{ Str::limit($item->bio, 20) }}</td>
        <td>
            <form action="/cast/{{$item->id}}" method="post">
              @csrf
              @method('delete')
              <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info mx-2">detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning mx-2">update</a>
              <input type="submit" value="delete" class="btn btn-sm btn-danger mx-2">
            </form>
        </td>
    </tr>
    @empty
    <tr>
        <td>tidak ada data</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection